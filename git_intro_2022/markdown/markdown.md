# I'm a huge section heading
some text
## I'm a large section heading
some text
### I'm a medium section heading
some text
#### I'm a small section heading
some text
#### I'm a small section heading
some text
##### I do not differ in size anymore
some text

# Iterations/Enumerations
* an item
* another item

1. first item
2. second item

# Paragraphs and line breaks
If you want to start a new paragraph...

... leave an empty line.

To force a line break (empty spaces are inserted after this)  
add two empty spaces in the end of the line.

# Coding
Let's use some `inline code`.

Or do it blockwise:
```
def(arg):
    return arg+1 
```

# Tables

| id  | col1 | col2 |
|-----|------|------|
| 1   | hs   | 123  |
| 2   | ts   | 321  |

# Images
![](img.png)

# Links
[I'm a web link to the git homepage](https://git-scm.com/)

[I'm a link to another file](anotherfile.md)


