# Introduction to R

**Disclaimer**: Everything that is being shown and discussed during the workshop is generic and not subject-specific. Please bear with me if the examples that I choose tend to be about sequence data processing. 

## Schedule

The workshop will consist of 2 sessions: The first session is focused more on the first steps in R and getting familiar with how R understands data. The second session is about more advanced data handling and scripting. There is also the option to work together on your own data in break-out rooms. To accommodate all participant's availability, each session will be offered twice. 

| | Morning (9-12) | Afternoon (13-16) |
| -- | -- | -- |
| Mon, 07.03.2022 | session 1 | break-out |
| Tue, 08.03.2022 | session 2 | session 1 (+break-out) |
| Wed, 09.03.2022 | break-out | |
| Thu, 10.03.2022 | session 2 | |


## Sessions (preliminary)

### Session 1:

* R command line and Rstudio
* R packages: CRAN, bioconductor, github
* R data and object types
* Data and object type conversions
* Navigating R objects

* Good coding practice

### Session 2:

* Reading data into R
* Rearranging R objects
* Loops
* Rudimentary data exploration (plots)
* Saving R objects and writing data to file
* Writing functions
* Tidyverse

## Course material:

* [Slides](https://git.io-warnemuende.de/bio_inf/tutorials_collection/src/branch/master/R_intro_2022/R_intro_slides.pdf)
* [Script](https://git.io-warnemuende.de/bio_inf/tutorials_collection/src/branch/master/R_intro_2022/R_intro_script.R)
* [Examples data](https://owncloud.io-warnemuende.de/index.php/s/9trizKclZJ9amBC)