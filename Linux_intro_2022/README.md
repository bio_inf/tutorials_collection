# Introduction to the linux command line

**Disclaimer**: Everything that is being shown and discussed during the workshop is generic and not subject-specific. Please bear with me if the examples that I choose tend to be about sequence data processing. 

## Schedule

The workshop will consist of 2 sessions: The first session will cover the basics and end with a set of exercises for the participants to solve until session 2. There is the option to work together on these exercises in break-out rooms. Session 2 will start with sharing the solutions to the exercises from session 1. Therefore, it is stronly recommended to not attend session 1 and 2 on the same day. To accommodate all participant's availability, each session will be offered twice. 

| | Morning (9-12) | Afternoon (13-16) |
| -- | -- | -- |
| Mon, 28.02.2022 | session 1 | break-out |
| Tue, 01.03.2022 | session 1 | session 2 (+break-out) |
| Thu, 03.03.2022 | session 2 | |


## Sessions

### Session 1:

* Finding a suitable terminal to work on
* User environment: default shell, bash (```.bashrc```), environment variables (```$PATH```)
* Basic linux commands: ```cd``` (file paths), ```pwd```, ```man```, ```ls``` (file permissions), ```echo``` (variables), ```cp```, ```mv```, ```mkdir```, ```rm```, ```ssh```, ```scp```, ```htop```, ```du```, ```df```, ```time```, ```less```, ```cut```, ```paste```, ```sort```, ```uniq```, ```diff```, ```cat```, ```hexdump```, ```wc```, ```tr```, ```dos2unix```, ```touch```, ```grep```, ```awk```, ```sed``` (regular expressions), ```vi```, ```xargs```, ```basename```,  ```ln```, ```wget```, ```md5sum```, ```gzip```, ```bzip2```, ```tar```, ```zless```, ```zcat```, ```zgrep```, ```screen```, etc.
* Standard input (stdin), standard output (stdout), standard error (stderr), redirection, pipes
* Autocomplete, copy/paste and recall previous commands
* Keyboard shortcuts
* Remote server connection
* Monitor resource usage

### Session 2:

* Open questions session 1 and exercises
* Loops for sequential processing (for and while loops)
* if/else statements
* Writing an executable bash script
* Software installation, conda, modules
* Q&A
* Resumable sessions
* SSH keys
* If there is time: parallelization, workflow managers (snakemake), resource managers (slurm) – demo only

## Course material:

* [Slides](https://git.io-warnemuende.de/bio_inf/tutorials_collection/src/branch/master/Linux_intro_2022/linux_intro_slides.pdf)
* [Example data](https://owncloud.io-warnemuende.de/index.php/s/MTi0e2nuP2slZ3J)
* [Advanced exercises](https://git.io-warnemuende.de/bio_inf/tutorials_collection/src/branch/master/Linux_intro_2022/tasks.sh)