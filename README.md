This repository contains a collection of introduction slides and bioinf tutorials.

The teaching materials for the PM9 module of the Marine Biology Master at Uni Rostock (microbial ecology) have been moved to a [separate repository](https://git.io-warnemuende.de/bio_inf/teaching_materials).

